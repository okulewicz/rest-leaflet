package pl.sages.gis.module7.dto;

import java.util.ArrayList;
import java.util.List;

public class PointsList {
    public final String type = "FeatureCollection";
    public final List<Point> features = new ArrayList<>();

    public void addPoint(Point point) {
        features.add(point);
    }

}
