package pl.sages.gis.module7.services;

import com.google.gson.Gson;
import pl.sages.gis.module7.dto.Point;
import pl.sages.gis.module7.dto.PointsList;

import javax.inject.Singleton;
import java.sql.*;

@Singleton
public class DBPointsService implements PointsService {
    private static final String CONNECTION_STR = "jdbc:postgresql://localhost:5432/rest_leaflet_db";
    private static final String USERNAME = "rest_gis";
    private static final String PASSWORD = "rest-gis";
    public static final String UPDATE_POINTS = "UPDATE points SET name= ?, geom = ST_GeomFromGeoJSON(?) WHERE id = ?;";
    public static final String SELECT_POINTS = "SELECT ST_AsGeoJSON(points,'geom') FROM points;";
    //CONCATENATION WITH + IS NOT OPTIMAL

    @Override
    public PointsList getPoints() {
        PointsList points = new PointsList();
        try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
            // create a Statement
            try (Statement stmt = conn.createStatement()) {
                //execute query
                try (ResultSet rs = stmt.executeQuery(SELECT_POINTS)) {
                    //position result to first
                    while (rs.next()) {
                        //System.out.println(rs.getString(1));
                        Point point = new Gson().fromJson(rs.getString(1), Point.class);
                        points.addPoint(point);
                    }
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return points;
    }

    @Override
    public void modifyPoint(Point point) {
            try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
                // create a Statement
                try (PreparedStatement stmt = conn.prepareStatement(UPDATE_POINTS)) {
                    //execute query
                    stmt.setString(1, point.properties.name);
                    stmt.setString(2, new Gson().toJson(point.geometry));
                    stmt.setLong(3, point.properties.id);
                    stmt.execute();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
    }
}
