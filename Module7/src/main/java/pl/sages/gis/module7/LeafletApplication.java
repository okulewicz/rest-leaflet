package pl.sages.gis.module7;

import io.micronaut.runtime.Micronaut;

public class LeafletApplication {

    public static void main(String[] args) {
        Micronaut.run(LeafletApplication.class, args);
    }
}
