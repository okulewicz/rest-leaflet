package pl.sages.gis.module7.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import pl.sages.gis.module7.services.PointsService;
import pl.sages.gis.module7.dto.Point;
import pl.sages.gis.module7.dto.PointsList;

@Controller("/points")
public class PointController {
    PointsService pointsService;

    public PointController(PointsService pointsService) {
        this.pointsService = pointsService;
    }

    @Get
    public HttpResponse<PointsList> getAllPoints() {
        PointsList points = pointsService.getPoints();
        return HttpResponse.ok(points);
    }

    @Put("/{id}")
    public HttpResponse<String> modifyPoint(@Body Point point, @QueryValue long id, @QueryValue String token ) {
        if ("0123456789".equals(token)) {
            pointsService.modifyPoint(point);
            return HttpResponse.ok("Point modified");
        } else {
            return HttpResponse.unauthorized();
        }
    }
}
