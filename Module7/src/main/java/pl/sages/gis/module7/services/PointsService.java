package pl.sages.gis.module7.services;

import pl.sages.gis.module7.dto.Point;
import pl.sages.gis.module7.dto.PointsList;

import javax.inject.Singleton;

@Singleton
public interface PointsService {
    PointsList getPoints();

    void modifyPoint(Point point);
}
