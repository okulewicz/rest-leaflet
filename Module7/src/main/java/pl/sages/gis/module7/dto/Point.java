package pl.sages.gis.module7.dto;

public class Point {
    public class Properties {
        public long id;
        public String name;
    }

    public class Geometry {
        public final String type = "Point";
        public double[] coordinates;
    }

    public final String type = "Feature";
    public Properties properties;
    public Geometry geometry;

    private Point() {

    }

    public Point(long id, String name, double lat, double lng) {
        this.properties = new Properties();
        properties.id = id;
        properties.name = name;
        this.geometry = new Geometry();
        this.geometry.coordinates = new double[2];
        this.geometry.coordinates[0] = lng;
        this.geometry.coordinates[1] = lat;
    }
}
