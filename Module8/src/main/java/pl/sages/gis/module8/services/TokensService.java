package pl.sages.gis.module8.services;

import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

@Singleton
public class TokensService {
    private final Connection connection;
    public static final String SELECT_FROM_USERS_WHERE_USERNAME =
            "SELECT id FROM users WHERE username = ? AND password = MD5(?);";
    public static final String INSERT_TOKEN =
            "INSERT INTO tokens (token, user_id) VALUES (?,?) RETURNING token;";
    public static final String CHECK_TOKEN = "SELECT username FROM users JOIN tokens ON users.id = tokens.user_id WHERE token = ?;";
    private final Random random;


    public TokensService(DBConnector dbConnector, UsersService usersService) throws SQLException {
        usersService.ensureTableExistence();
        connection = dbConnector.getConnection();
        random = new Random();
    }

    public String getToken(String username, String password) {
        try {
            PreparedStatement checkIfUserExists =
                    connection.prepareStatement(SELECT_FROM_USERS_WHERE_USERNAME);
            checkIfUserExists.setString(1, username);
            checkIfUserExists.setString(2, password);
            ResultSet results = checkIfUserExists.executeQuery();
            if (results.next()) {
                int userId = results.getInt(1);
                byte[] randomToken = generateToken();
                PreparedStatement createToken = connection.prepareStatement(INSERT_TOKEN);
                createToken.setString(1, new String(randomToken));
                createToken.setInt(2, userId);
                ResultSet tokenResults = createToken.executeQuery();
                if (tokenResults.next()) {
                    return tokenResults.getString(1);
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (SQLException throwable) {
            return null;
        }
    }

    private byte[] generateToken() {
        byte[] randomToken = new byte[32];
        for (int idx = 0; idx < randomToken.length; ++idx) {
            randomToken[idx] = (byte)(65 + random.nextInt(26));
        }
        return randomToken;
    }

    public String getUsernameForToken(String token) {
        try {
            PreparedStatement checkIfUserExists =
                    connection.prepareStatement(CHECK_TOKEN);
            checkIfUserExists.setString(1, token);
            ResultSet results = checkIfUserExists.executeQuery();
            if (results.next()) {
                return results.getString(1);
            } else {
                return null;
            }
        } catch (SQLException throwable) {
            return null;
        }
    }
}
