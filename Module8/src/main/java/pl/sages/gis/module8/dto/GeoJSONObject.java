package pl.sages.gis.module8.dto;

import java.util.Map;

public class GeoJSONObject {
    /*
    public class Properties {
        public long id;
        public String name;
    }

    public class Geometry {
        public final String type = "GeoJSONObject";
        public double[] coordinates;
    }
    */
    public final String type = "Feature";
    public Map<String, Object> properties;
    public Map<String, Object> geometry;

}
