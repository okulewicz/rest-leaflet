package pl.sages.gis.module8.services;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import java.sql.*;

@Singleton
public class UsersService {

    public static final String SELECT_FROM_USERS_WHERE_USERNAME =
            "SELECT * FROM users WHERE username = ?;";
    public static final String INSERT_INTO_USERS =
            "INSERT INTO users (username,password) VALUES(?,MD5(?))  RETURNING id;";
    public static final String CREATE_TABLE_USERS = String.join(" ", new String []
            { "CREATE TABLE IF NOT EXISTS users(",
            "id SERIAL PRIMARY KEY,",
            "username VARCHAR NOT NULL,",
            "password VARCHAR NOT NULL",
            ");"});
    public static final String CREATE_TABLE_TOKENS = String.join(" ", new String []
            { "CREATE TABLE IF NOT EXISTS tokens(",
                    "id SERIAL PRIMARY KEY,",
                    "token VARCHAR NOT NULL,",
                    "user_id integer NOT NULL,",
                    "CONSTRAINT fk_user",
                    "FOREIGN KEY(user_id)",
                    "REFERENCES users(id)",
                    ");"});

    private final Connection dbConnection;

    public UsersService(DBConnector connector) {
        dbConnection = connector.getConnection();
    }

    @PostConstruct
    public void ensureTableExistence() throws SQLException {
        PreparedStatement tryCreateTableUsers =
                dbConnection.prepareStatement(CREATE_TABLE_USERS);
        tryCreateTableUsers.execute();
        PreparedStatement tryCreateTableTokens =
                dbConnection.prepareStatement(CREATE_TABLE_TOKENS);
        tryCreateTableTokens.execute();
    }

    public int addUser(String username, String password) {
        try {
            PreparedStatement checkIfUserExists =
                    dbConnection.prepareStatement(SELECT_FROM_USERS_WHERE_USERNAME);
            checkIfUserExists.setString(1, username);
            ResultSet results = checkIfUserExists.executeQuery();
            if (results.next()) {
                return -1;
            }
            PreparedStatement insertUser =
                    dbConnection.prepareStatement(INSERT_INTO_USERS);
            insertUser.setString(1, username);
            insertUser.setString(2, password);
            ResultSet resultId = insertUser.executeQuery();
            if (resultId.next()) {
                return resultId.getInt(1);
            } else {
                return -1;
            }

        } catch (SQLException throwable) {
            return -1;
        }
    }

}
