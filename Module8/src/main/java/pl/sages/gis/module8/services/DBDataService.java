package pl.sages.gis.module8.services;

import com.google.gson.Gson;
import pl.sages.gis.module8.dto.GeoJSONObject;
import pl.sages.gis.module8.dto.ObjectsList;

import javax.inject.Singleton;
import java.sql.*;

@Singleton
public class DBDataService implements DataService {
    public static final String UPDATE_BEGIN = "UPDATE ";
    public static final String UPDATE_FINISH = "geom = ST_GeomFromGeoJSON(?) WHERE id = ?;";
    private final DBConnector connector;

    public DBDataService(DBConnector connector) {
        this.connector = connector;
    }

    @Override
    public ObjectsList getObjects(String tableName) {
        ObjectsList points = new ObjectsList();
        Connection conn = connector.getConnection();
        try (Statement stmt = conn.createStatement()) {
            //DANGER!!! SQL INJECTION AHEAD
            String selectData = String.join("",
                    new String[]{"SELECT ST_AsGeoJSON(\"",
                            tableName,
                            "\",'geom') FROM \"",
                            tableName,
                            "\";"});
            //execute query
            try (ResultSet rs = stmt.executeQuery(selectData)) {
                //position result to first
                while (rs.next()) {
                    //System.out.println(rs.getString(1));
                    String geoJSONAsString = rs.getString(1);
                    GeoJSONObject geoJSONObject = new Gson().fromJson(geoJSONAsString, GeoJSONObject.class);
                    points.addPoint(geoJSONObject);
                }
            } catch (SQLException throwable) {
                System.err.println("Query:" + selectData + " FAILED");
                //throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return points;
    }

    @Override
    public void modifyObject(String tableName, GeoJSONObject geoJSONObject) {
        Connection conn = connector.getConnection();
        // create a Statement
        StringBuilder stringSQL = new StringBuilder();
        stringSQL.append(UPDATE_BEGIN);
        //table name
        //DANGER! NEED TO VALIDATE AGAINST SCHEMA!
        stringSQL.append(tableName);
        stringSQL.append(" SET ");
        for (String key : geoJSONObject.properties.keySet()) {
            if (!key.equals("id")) {
                //column name
                stringSQL.append("\"");
                //DANGER! NEED TO VALIDATE AGAINST SCHEMA!
                stringSQL.append(key);
                stringSQL.append("\" = ?, ");
            }
        }
        stringSQL.append(UPDATE_FINISH);
        try (PreparedStatement stmt = conn.prepareStatement(stringSQL.toString())) {
            //execute query
            int count = 1;
            for (String key : geoJSONObject.properties.keySet()) {
                if (!key.equals("id")) {
                    //need to check types against DB schema
                    stmt.setObject(count++, geoJSONObject.properties.get(key));
                }

            }
            stmt.setString(count, new Gson().toJson(geoJSONObject.geometry));
            stmt.setLong(count + 1, Long.parseLong(geoJSONObject.properties.get("id").toString()));
            stmt.execute();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }
}
