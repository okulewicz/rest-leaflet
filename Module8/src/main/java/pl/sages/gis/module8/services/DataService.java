package pl.sages.gis.module8.services;

import pl.sages.gis.module8.dto.GeoJSONObject;
import pl.sages.gis.module8.dto.ObjectsList;

import javax.inject.Singleton;

@Singleton
public interface DataService {
    ObjectsList getObjects(String tableName);

    void modifyObject(String tableName, GeoJSONObject geoJSONObject);
}
