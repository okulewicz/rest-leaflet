package pl.sages.gis.module8.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import pl.sages.gis.module8.services.UsersService;

@Controller("/users")
public class UsersController {
    private final UsersService userService;

    UsersController(UsersService usersService) {
        this.userService = usersService;
    }

    @Post
    @Consumes(value = MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(value = "text/plain;charset=utf-8")
    public HttpResponse<String> addNewUser(String username, String password, String secret) {
        if ("admin-secret".equals(secret)) {
            if (userService.addUser(username, password) >= 0) {
                return HttpResponse.created("Użytkownik dodany.");
            } else {
                return HttpResponse.badRequest("Użytkownik nie został dodany.");
            }
        } else {
            return HttpResponse.badRequest("Brak uprawnień.");
        }
    }

}
