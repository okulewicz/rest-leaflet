package pl.sages.gis.module8.dto;

import java.util.ArrayList;
import java.util.List;

public class ObjectsList {
    public final String type = "FeatureCollection";
    public final List<GeoJSONObject> features = new ArrayList<>();

    public void addPoint(GeoJSONObject geoJSONObject) {
        features.add(geoJSONObject);
    }

}
