package pl.sages.gis.module8;

import io.micronaut.runtime.Micronaut;

public class LeafletApplication {

    public static void main(String[] args) {
        Micronaut.run(LeafletApplication.class, args);
    }
}
