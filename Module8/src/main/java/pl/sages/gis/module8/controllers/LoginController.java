package pl.sages.gis.module8.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import pl.sages.gis.module8.services.TokensService;

@Controller("/login")
public class LoginController {
    private final TokensService tokensService;

    public LoginController(TokensService tokensService) {
        this.tokensService = tokensService;
    }

    @Post
    @Consumes(value = MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(value = "text/plain;charset=utf-8")
    public HttpResponse<String> getToken(String username, String password) {
        String token = tokensService.getToken(username, password);
        if (token != null) {
            return HttpResponse.created(token);
        } else {
            return HttpResponse.unauthorized();
        }
    }

    @Get
    @Produces(value = "text/plain;charset=utf-8")
    public HttpResponse<String> getUsername(@QueryValue String token) {
        String username = tokensService.getUsernameForToken(token);
        if (token != null) {
            return HttpResponse.ok(username);
        } else {
            return HttpResponse.notFound();
        }
    }
}
