package pl.sages.gis.module8.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import pl.sages.gis.module8.dto.GeoJSONObject;
import pl.sages.gis.module8.dto.ObjectsList;
import pl.sages.gis.module8.services.DataService;
import pl.sages.gis.module8.services.TokensService;

@Controller("/data")
public class DataController {
    private final TokensService tokensService;
    DataService dataService;

    public DataController(DataService dataService, TokensService tokensService) {
        this.dataService = dataService;
        this.tokensService = tokensService;
    }

    //DANGER!!!
    @Get("/{table}")
    public HttpResponse<ObjectsList> getAllData(@QueryValue String table, String token) {
        if (tokensService.getUsernameForToken(token) != null) {
            ObjectsList points = dataService.getObjects(table);
            return HttpResponse.ok(points);
        } else {
            return HttpResponse.unauthorized();
        }
    }

    @Put("/{table}/{id}")
    public HttpResponse<String> modifyData(@Body GeoJSONObject geoJSONObject,
                                           @QueryValue long id,
                                           @QueryValue String token,
                                           @QueryValue String table) {
        if (tokensService.getUsernameForToken(token) != null) {
            dataService.modifyObject(table,geoJSONObject);
            return HttpResponse.ok("GeoJSONObject modified");
        } else {
            return HttpResponse.unauthorized();
        }
    }
}
