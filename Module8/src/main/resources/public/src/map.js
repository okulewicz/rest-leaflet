var SWECO = new Object();
SWECO.ICONS = new Object();

SWECO.ICONS.redIcon = new L.icon({
    iconUrl: 'styles/images/red.png',
    iconSize: [18, 30],
    iconAnchor: [9, 30],
    popupAnchor: [0, -30],
});

SWECO.ICONS.orangeIcon = new L.icon({
    iconUrl: 'styles/images/orange.png',
    iconSize: [18, 29],
    iconAnchor: [9, 29],
    popupAnchor: [0, -29],
});

/*
        var popupContent = '<h1 class="leaflet-popup-content_header">I am a GeoJSON ' +
                    feature.geometry.type + '</h1>';
        if (feature.properties && feature.properties.id) {
                popupContent += '<div class="leaflet-popup-content_body"><strong>Id:</strong> ' + feature.properties.id + '</div>';
        }
        marker.bindPopup(popupContent);
*/
//this sets initializeApplication an entry point for javascript application
document.addEventListener( "DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    loadMap();
}

function loadMap() {
    if(SWECO.map) {
        SWECO.map.remove();
    }
    SWECO.map = L.map('map-container', {editable: true});
    SWECO.map.setView([52.05, 20.00], 6);
    var osmBaseLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    })
    osmBaseLayer.addTo(SWECO.map);
}

function loadAuthorizedMap() {
    loadMap();
    SWECO.layerControl = L.control.layers(null, null/*, { 'collapsed': false }*/)
    SWECO.layerControl.addTo(SWECO.map);

    fetch('data/points?token='+SAGES.token)
        .then(response => response.json())
        .then(data => addPointsToMap(data));

    addPolylinesToMap(json_siecirealizowane_4);
    //var kanalizacja = L.geoJSON(json_kanalizacjasanitarna_3);
    //kanalizacja.addTo(SWECO.map);
}

function addPolylinesToMap(data) {
    var polylines = L.layerGroup();
    for (var i = 0; i < data.features.length; ++i) {
        var feature = data.features[i];
        //https://leafletjs.com/reference-1.0.3.html#geojson-coordstolatlngs
        var coordinates = L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates,1);
        var polyline = L.polyline(coordinates);
        polyline.properties = feature.properties;
        polyline.addTo(polylines);
    }
    polylines.addTo(SWECO.map);
    //this needs to be done after adding to map
    polylines.eachLayer(function (line) {
        line.on('click', function() {
        //http://leaflet.github.io/Leaflet.Editable/doc/api.html#editablemixin
            this.toggleEdit();
            if (!this.editEnabled()) {
                var geoJSON = this.toGeoJSON();
                geoJSON.properties = this.properties;
                console.log(geoJSON);
                var dataContainer = document.getElementById('data-container');
                dataContainer.innerHTML = '';
            } else {
                generatePropertiesTable(this);
            }
        });
    });
    SWECO.layerControl.addOverlay(polylines, 'Kanalizacja');
}

function addPointsToMap(data) {
    var pois = L.layerGroup();
    for (var i = 0; i < data.features.length; ++i) {
        console.log(data.features[i]);
        var feature = data.features[i];
        var marker = L.marker([
                    feature.geometry.coordinates[1],
                    feature.geometry.coordinates[0]
                ]);
        
        marker.properties = feature.properties;
        marker.setStyle = function() {
            if (this.properties.name == 'fabryka') {
                this.setIcon(SWECO.ICONS.redIcon);
            } else {
                this.setIcon(SWECO.ICONS.orangeIcon);
            }
        };
        marker.sendToDB = function() {
                   var geoJSON = this.toGeoJSON();
                   geoJSON.properties = this.properties;
                   fetch('data/points/'+geoJSON.properties.id+'?token=' + SAGES.token,{
                       body: JSON.stringify(geoJSON),
                       method: 'PUT',
                       headers: {
                               'Content-Type': 'application/json'
                           }
                       });
               };

        marker.setStyle();
        marker.on('click', function(e) {
            var geoJSON = this.toGeoJSON();
            geoJSON.properties = this.properties;
            console.log(geoJSON);
            generatePropertiesTable(this);
            //pois.removeLayer(this);
        });
        marker.on('editable:dragend', function (e) {
            this.sendToDB();
        });
        marker.addTo(pois);
    }
    pois.addTo(SWECO.map);
    //this needs to be done after adding to map
    pois.eachLayer(function (marker) {
        marker.enableEdit();
    });
    SWECO.layerControl.addOverlay(pois, 'POI');
    /*
    for (var i = 0; i < markerArray.length; ++i) {
        markerArray[i].enableEdit();
    }
    */
    /*
    var pointsLayer = L.geoJSON(data, {
        onEachFeature: generateMarkerOnMap(mymap)
    });
    */
}

/*
function styleDefininition(feature) {
    switch (feature.properties.id) {
        case 1: return {color: "red"};
        case 2: return {color: "blue"};
    }
}
*/

function generatePropertiesTable(marker) {
    var dataContainer = document.getElementById('data-container');
    dataContainer.innerHTML = '';
    var table = document.createElement('table');
    var tr = document.createElement('tr');
    var propertyTh = document.createElement('th');
    var propertyValueTh = document.createElement('th');
    var propertyName = document.createTextNode('Atrybut');
    var propertyValue = document.createTextNode('Wartość');
    propertyTh.appendChild(propertyName);
    propertyValueTh.appendChild(propertyValue);
    tr.appendChild(propertyTh);
    tr.appendChild(propertyValueTh);
    table.appendChild(tr);
    for (var property in marker.properties) {
        var tr = document.createElement('tr');
        var propertyTd = document.createElement('td');
        var propertyValueTd = document.createElement('td');
        var propertyValueInput;
        if (property != 'id') {
            propertyValueInput = document.createElement('input');
            propertyValueInput.value = marker.properties[property];
            propertyValueInput.propertyName = property;
        } else {
            propertyValueInput = document.createTextNode(marker.properties[property]);
        }
        var propertyName = document.createTextNode(property);
        propertyTd.appendChild(propertyName);
        propertyValueInput.onchange = function (event) {
            marker.properties[this.propertyName] = this.value;
            marker.setStyle();
            marker.sendToDB();
        }
        propertyValueTd.appendChild(propertyValueInput);
        tr.appendChild(propertyTd);
        tr.appendChild(propertyValueTd);
        table.appendChild(tr);
    }
    dataContainer.appendChild(table);
}

