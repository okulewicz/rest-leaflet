var SAGES = new Object();
SAGES.token = "";

SAGES.initializeApplication = function() {
    document.getElementById('login-form').onsubmit = function() {
        var username = document.getElementById('username-input').value;
        var password = document.getElementById('password-input').value;
        fetch('login', {
            method: 'POST',
            body: 'username='+username+'&password='+password,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            if (response.ok) return response.text();
        })
        .then(function(token) {
            SAGES.token = token;
            loadAuthorizedMap();
            fetch('login?token='+SAGES.token)
            .then(response => response.text())
            .then(function(username) {
                document.getElementById('login-panel').innerHTML =
                 'Witaj, ' + username;
            });
        });
        return false;
    }
}

document.addEventListener( "DOMContentLoaded", SAGES.initializeApplication, false);

