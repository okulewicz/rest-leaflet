# README #

Repozytorium na potrzeby 3-dniowego szkolenia z zakresu serwisów REST i ich wykorzystania w bibliotece mapowej Leaflet.

## Plan szkolenia ##

 * [Wprowadzenie do języka Java: zmienne, obiekty, operacje na danych](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module1/JavaClasses)
 * [Serializacja obiektów](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module2/ObjectSerialization)
 * [Tworzenie serwisów REST w oparciu o bibliotekę Micronaut](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module3/MicronautIntroduction)
 * [Komunikacja aplikacji przeglądarkowych z serwisami REST w oparciu o zapytania AJAX](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module4/AjaxRequests2Rest)
 * [Integracja serwisów REST z biblioteką Leaflet](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module5/Leaflet)
 * [Edytowalne warstwy danych w bibliotece Leaflet](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module6/LeafletEdit)
 * [Komunikacja z bazą danych PostgreSQL w technologii Java](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module7/Save2DB)
 * [Autoryzacja i identyfikacja użytkowników w serwisach REST](https://bitbucket.org/okulewicz/rest-leaflet/wiki/Module8/Authorize)
 
## Maszyna wirtualna ##

* [Plik ustawień](https://bit.ly/2CpUggo)
* [Dysk maszyny](https://bit.ly/33Qnd03)

## Tutorial ##

 * [Wiki](https://bitbucket.org/okulewicz/rest-leaflet/wiki)
 
## Źródła ##

### AJAX i REST ###

 * [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
 * [Parametry funkcji fetch](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch#Parameters)
 * [Typowe błędy w użyciu fetch](http://blog.niftysnippets.org/2018/06/common-fetch-errors.html)
 * [Serwisy REST w PHP](https://phppot.com/php/php-restful-web-service/)

### Micronaut ###
 
 * [Instalacja Micronaut (i innych SDK)](https://sdkman.io/sdks)
 * [Tworzenie aplikacji](https://docs.micronaut.io/snapshot/guide/index.html#cli)
 * [Pierwszy kontroler](https://docs.micronaut.io/snapshot/guide/index.html#creatingServer)
 * [Statyczne pliki](https://guides.micronaut.io/micronaut-spa-react/guide/index.html#staticResources)
 * [Identyfikacja w oparciu o ciastka](https://guides.micronaut.io/micronaut-security-jwt-cookie/guide/index.html)
 
### Leaflet ###

 * [Przykład wykorzystaia GeoJSONa](https://leafletjs.com/examples/geojson/example.html)
 * [Pobranie GeoJSONa z wykorzystaniem fetch](https://gis.stackexchange.com/a/358446/71094)
 * [Zmiana stylu elementów warstwy](https://gis.stackexchange.com/q/265648/71094)
 * [Stylizowane warstwy z GeoJSONa z obsługą kliknięć](https://www.e-education.psu.edu/geog585/node/769)
 * [Dokumentacja wtyczki Leaflet.Editable](http://leaflet.github.io/Leaflet.Editable/)
 * [Przykład użycia wtyczki Leaflet.Editable](http://leaflet.github.io/Leaflet.Editable/example/index.html)
 * [Sposoby na użycie wtyczki Leaflet.Editable z GeoJSONem](https://gis.stackexchange.com/questions/237171/making-a-geojson-layer-editable-with-the-leaflet-editable-plugin)
 * [Sposoby na użycie wtyczki Leaflet.draw z GeoJSONem](https://gis.stackexchange.com/questions/203540/how-to-edit-an-existing-layer-using-leaflet)
 * [Przykłady użycia Leaflet](http://www.mini.pw.edu.pl/~okulewiczm/leaflet/)
 
### PostGIS ###
 
  * [Instalacja](https://trac.osgeo.org/postgis/wiki/UsersWikiPostGIS24UbuntuPGSQL10Apt)
  * [Tworzenie GeoJSONów z wierszy - uproszczone](https://postgis.net/docs/ST_AsGeoJSON.html)
  * [Tworzenie GeoJSONów z wierszy - skomplikowane](https://www.postgresonline.com/article_pfriendly/267.html)
  * [Tworzenie geometrii z GeoJSONa](https://postgis.net/docs/ST_GeomFromGeoJSON.html)
  * [Przykłady GeoJSONów](https://geojson.org/geojson-spec.html)
  * [Zwracanie id po utworzeniu wiersza](https://www.postgresqltutorial.com/postgresql-serial/)
 
### Narzędzia ###
 
 * [Instalacja QGISa](https://www.qgis.org/pl/site/forusers/alldownloads.html#debian-ubuntu) 
 
## Ściąga poleceń ##

### Terminal

 * Pobranie schematu tabeli: `pg_dump -st tablename dbname`
 * Podłączenie się do bazy projektu: `sudo -u rest_gis psql -d rest_leaflet_db`
 * Tworzenie nowego projektu Micronaut: `mn create-app package.ProjectName` 
 
### IntelliJ

 * Odświeżenie gradle'a: `Ctrl+Shift+O`
 * Wyłączenie kodu w metodę: `Ctrl+Alt+M`
 * Zmiana nazwy: `Shift+F6`
 * Podpowiedzi w rozwiązywaniu problemów: `Alt+Enter`
 * Pokaż/ukryj projekt: `Alt+1`
 * commit: `Ctrl+K`
 * push from commit: `Ctrl+Alt+K`
 * push: `Ctrl+Shift+K`
 * stop application: `Ctrl+F2`
 * re-run last configuration: `Ctrl+F5`
 
### Maven

 * `compile group: 'org.postgresql', name: 'postgresql', version: '42.2.14'`
 * `compile group: 'com.google.code.gson', name: 'gson', version: '2.8.6'`
 *  `mavenCentral()` -> `maven {url "http://insecure.repo1.maven.org/maven2/"}`
 *  `jcenter()` -> `maven {url "http://jcenter.bintray.com"}`
 
### VM
 
 * Pełny ekran: `RightCtrl+F`
 * Menu maszyny: `RightCtrl+Home`
 * Zrzut ekranu maszyny: `RightCtrl+E`