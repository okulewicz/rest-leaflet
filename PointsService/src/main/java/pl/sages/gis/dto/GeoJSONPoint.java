package pl.sages.gis.dto;

/**
 * class for representing points in GeoJSON format
 */
public class GeoJSONPoint {
    public String type = "Feature";
    public GeoJSONGeometry geometry;
    public GeoJSONProperties properties;
}
