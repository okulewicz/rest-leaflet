package pl.sages.gis.dto;

/**
 * Class for sample GeoJSON properties
 */
public class GeoJSONProperties {
    public int id;
    public String name;
    public String type;
}
