package pl.sages.gis.dto;

/**
 * Class for GeoJSON geometry
 */
public class GeoJSONGeometry {
    public String type;
    public double[] coordinates;
}
