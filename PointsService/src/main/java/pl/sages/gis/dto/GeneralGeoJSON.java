package pl.sages.gis.dto;

import java.util.Map;

public class GeneralGeoJSON {
    public String type;
    public Map<String, Object> properties;
    public Map<String, Object> geometry;
}
