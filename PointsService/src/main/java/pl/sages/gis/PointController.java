package pl.sages.gis;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import pl.sages.gis.dto.GeoJSONGeometry;
import pl.sages.gis.dto.GeoJSONPoint;
import pl.sages.gis.dto.GeoJSONProperties;

import java.util.Map;

/**
 *
 */
//This annotation defines the HTTP endpoint where controller will be accessible
@Controller("/points")
public class PointController {
    //this annotation marks the HTTP method mapped to the Java method
    @Get
    HttpResponse<GeoJSONPoint> getUsers() {
        //we wrap up the point in HTTP Response to get HTTP Status Code
        GeoJSONPoint point = new GeoJSONPoint();
        point.type = "Feature";
        point.geometry = new GeoJSONGeometry();
        point.geometry.coordinates = new double[2];
        point.geometry.coordinates[0] = 52;
        point.geometry.coordinates[1] = 14;
        point.properties = new GeoJSONProperties();
        point.properties.id = 1;
        point.properties.name = "nazwa";
        point.properties.type = "jakiś typ";
        return HttpResponse.ok(point);
    }
}