package pl.sages.gis;

import io.micronaut.runtime.Micronaut;

public class Application {
    /**
     * Starts the micronaut's server
     * @param args runtime parameters
     */
    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
