package pl.sages.gis;

import com.google.gson.Gson;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import pl.sages.gis.dto.GeneralGeoJSON;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Controller("/kanalizacja")
public class KanalizacjaController {
    private static final String CONNECTION_STR = "jdbc:postgresql://localhost:5432/rest_leaflet_db";
    private static final String USERNAME = "rest_gis";
    private static final String PASSWORD = "rest-gis";

    private static final String INSERT_KANALIZACJA =
        "INSERT INTO public.kanalizacja (msc,mk,uwagi,km,mk_km,domy,budowa,geom)" +
                " VALUES (?,?,?,?,?,?,?,ST_GeomFromGeoJSON(?)) RETURNING id;";
    private static final String UPDATE_KANALIZACJA =
            "UPDATE public.kanalizacja SET msc = ?, mk = ?,uwagi = ?,km = ?, mk_km = ?," +
                    " domy = ?,budowa = ? ,geom = ST_GeomFromGeoJSON(?)" +
                    " WHERE id = ?;";
    private static final String DELETE_KANALIZACJA =
            "DELETE FROM public.kanalizacja WHERE id = ?;";
    private static final String SELECT_KANALIZACJA =
            "SELECT ST_AsGeoJSON(kanalizacja) FROM public.kanalizacja;";

    @Get
    HttpResponse<List<GeneralGeoJSON>> listObjects() {
        List<GeneralGeoJSON> objects = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
            // create a Statement
            try (PreparedStatement stmt = conn.prepareStatement(SELECT_KANALIZACJA)) {
                //execute query
                //stmt.execute();
                try (ResultSet rs = stmt.executeQuery()) {
                    //position result to first
                    while (rs.next()) {
                        String geoJsonAsString = rs.getString(1);
                        Gson gson = new Gson();
                        GeneralGeoJSON geoObject = gson.fromJson(
                                geoJsonAsString,
                                GeneralGeoJSON.class);
                        objects.add(geoObject);
                    }
                    return HttpResponse.ok(objects);
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return HttpResponse.notFound();
    }

    @Post
    @Produces(value = "text/plain;charset=utf-8")
    HttpResponse<Integer> addObject(GeneralGeoJSON geoJson) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
            // create a Statement
            try (PreparedStatement stmt = conn.prepareStatement(INSERT_KANALIZACJA)) {
                //execute query
                fillInStatement(geoJson, stmt);
                //stmt.execute();
                try (ResultSet rs = stmt.executeQuery()) {
                    //position result to first
                    if (rs.next()) {
                        //get id from RETURNING id;
                        int identifier = rs.getInt(1);
                        //send id to browser
                        return HttpResponse.ok(identifier);
                    }
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return HttpResponse.badRequest();
        /*
        System.out.println(geoJson.type);
        System.out.println(geoJson.properties);
        System.out.println(geoJson.geometry);
        System.out.println(geoJson.properties.get("budowa"));
        System.out.println(geoJson.properties.get("MSC"));
        */

    }

    @Put
    @Produces(value = "text/plain;charset=utf-8")
    HttpResponse<Integer> modifyObject(GeneralGeoJSON geoJson) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
            // create a Statement
            try (PreparedStatement stmt = conn.prepareStatement(UPDATE_KANALIZACJA)) {
                //execute query
                fillInStatement(geoJson, stmt);
                int id = Integer.parseInt(geoJson.properties.get("id").toString());
                stmt.setInt(9, id);
                //stmt.execute();
                return HttpResponse.ok(id);
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return HttpResponse.badRequest();
    }

    private void fillInStatement(GeneralGeoJSON geoJson, PreparedStatement stmt) throws SQLException {
        stmt.setObject(1, geoJson.properties.get("MSC"));
        stmt.setObject(2, Integer.parseInt(geoJson.properties.get("MK").toString()));
        stmt.setObject(3, geoJson.properties.get("uwagi"));
        stmt.setObject(4, Double.parseDouble(geoJson.properties.get("KM").toString()));
        stmt.setObject(5, Double.parseDouble(geoJson.properties.get("MK/KM").toString()));
        stmt.setObject(6, Integer.parseInt(geoJson.properties.get("domy").toString()));
        if (geoJson.properties.get("budowa") != null) {
            stmt.setObject(7, geoJson.properties.get("budowa"));
        } else {
            stmt.setObject(7, "");
        }
        Gson gson = new Gson();
        stmt.setObject(8, gson.toJson(geoJson.geometry));
    }

    @Delete("/{id}")
    public HttpResponse<String> deleteObject(@QueryValue int id) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
            // create a Statement
            try (PreparedStatement stmt = conn.prepareStatement(DELETE_KANALIZACJA)) {
                //execute query
                stmt.setInt(1, id);
                //stmt.execute();
                stmt.execute();
                return HttpResponse.ok("Obiekt " + id + " został usunięty");
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return HttpResponse.badRequest("Obiekt " + id + " nie został usunięty");
    }

}
