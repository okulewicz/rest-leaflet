//this sets initializeApplication an entry point for javascript application
document.addEventListener("DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    //initialize points download
    fetch('points', {
        method: 'GET',
        //when response will be ready it will be processed
    }).then(processResponse);
}

//processes response (http status, headers etc.)
function processResponse(response) {
    console.log(response);
    response.json().then(logJSON);
}

//processes actual data
function logJSON(data) {
    console.log(data);
    document.getElementById('status-loader').innerHTML = JSON.stringify(data);
}

