document.addEventListener("DOMContentLoaded", initializeApplication, false);
var SWECO = new Object();

function initializeApplication() {
    //enable editing on map
    var mymap = L.map('map-container', {editable: true}).setView([51.505, 16], 13);
    var osmBaseLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    })
    osmBaseLayer.addTo(mymap);
    //wczytujemy 
    var geoJSONLayer = L.geoJSON(json_siecirealizowane_4);
    //geoJSONLayer.addTo(mymap);
    //https://leafletjs.com/reference-1.6.0.html#path-option
    geoJSONLayer.setStyle({color: 'red', weight: 5});
    mymap.fitBounds(geoJSONLayer.getBounds());
    //mymap.zoomToBounds(geoJSONLayer.getBounds());
    //console.log(geoJSONLayer.getBounds());

    //grupowanie linii, dla ułatwienia obsługi warstw
    var polylines = L.layerGroup();
    for (var i = 0; i < json_siecirealizowane_4.features.length; ++i) {
        //wykorzystanie pliku json_siecirealizowane_4.js
        var feature = json_siecirealizowane_4.features[i];
        //https://leafletjs.com/reference-1.0.3.html#geojson-coordstolatlngs
        var coordinates = L.GeoJSON.coordsToLatLngs(feature.geometry.coordinates,1);
        var polyline = L.polyline(coordinates);
        //kopiowanie properties z pobranego GeoJSONa do obiektu reprezentującego linię w Leaflet
        polyline.properties = feature.properties;
        polyline.addTo(polylines);
    }
    polylines.addTo(mymap);
    //wykonujemy tę operację po dodaniu grupy do mapy
    polylines.eachLayer(setupLineEvents);
    SWECO.polylines = polylines;
}

function setupLineEvents(line) {
    //obsługa kliknięcia w linię
    line.on('click', function(event) {
        if (event.originalEvent.ctrlKey && this.properties.id != null) {
            deleteObject(this);
        } else {
            console.log(this);
            console.log(event);
            //w ramach kliknięcia przełącza możliwość edytowania
            //enableEdit i toggleEdit są możliwe dopiero po dodaniu do mapy
            this.toggleEdit();
        }
    });
    //obsługa zakończenia edycji
    line.on('editable:disable', function(event) {
        sendToServer(this);
    });
}

function deleteObject(line) {
    var fetchOptions = new Object();
    fetchOptions.method = 'DELETE';
    var geoObject = line;
    //wysyłamy dane do serwera
    fetch('kanalizacja/'+line.properties.id, fetchOptions)
    .then(function (response) {
        if (response.ok) {
            return response.text();
        }
    })
    .then(function (info) {
        console.log(info);
        SWECO.polylines.removeLayer(line);
    });
}

function sendToServer(line) {
    console.log('edycja zakończona');
    console.log(line.toGeoJSON());
    //przygotowanie parametrów zapytania
    var fetchOptions = new Object();
    var geoJSON = line.toGeoJSON();
    //uzupełniamy go o właściwości przechowane wraz z linią
    geoJSON.properties = line.properties;
    //całość dodajemy do ciała wiadomości HTTP
    fetchOptions.body = JSON.stringify(geoJSON);
    //informujemy serwer że dostanie JSONa
    fetchOptions.headers = {
                'Content-Type': 'application/json'
            };
    if (geoJSON.properties.id == null) {
        fetchOptions.method = 'POST';
    } else {
        fetchOptions.method = 'PUT';
    }
    //tworzymy obiekt GeoJSON zawierający geometrię
    //wysyłamy dane do serwera
    fetch('kanalizacja', fetchOptions)
    .then(function (response) {
        if (response.ok) {
            return response.text();
        }
    })
    .then(function (id) {
        console.log(id);
        line.properties.id = Number(id);
    });

}