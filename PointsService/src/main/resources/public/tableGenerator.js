document.addEventListener("DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    console.log(json_siecirealizowane_4.features);
    var propertyTable = document.createElement('table');

    var firstRowProperties = json_siecirealizowane_4.features[0].properties;
    var trFirstRow = document.createElement('tr');
    for (var property in firstRowProperties) {
        var td = document.createElement('th');
        td.innerHTML = property;
        trFirstRow.appendChild(td);
    }
    propertyTable.appendChild(trFirstRow);

    for (var idx = 0; idx < json_siecirealizowane_4.features.length; idx++) {
        var properties = json_siecirealizowane_4.features[idx].properties;
        var tr = document.createElement('tr');
        for (var property in properties) {
            var td = document.createElement('td');
            //replace works on Strings so we need to check type
            if (typeof properties[property] === 'string' || properties[property] instanceof String) {
                //corrects bad character encoding
                td.innerHTML = properties[property]
                    .replace('Ĺ›','ś')
                    .replace('Ĺ„','ń')
                    .replace('Ä™','ę')
                    .replace('Ĺ‚','ł')
                    ;
            } else {
                td.innerHTML = properties[property];
            }
            tr.appendChild(td);
            console.log(property + '=' + properties[property]);
        }
        propertyTable.appendChild(tr);

    }
    document.body.appendChild(propertyTable);
    //console.log(json_siecirealizowane_4.features[0].properties["MSC"]);
    /*
        console.log(document.body.childNodes);
        //reprezentuje stronę HTML w JS
        document;
        //tworzenie nowego elementu HTML w JS
        document.createElement('table');
    
        //tworzenie akapitu wraz z treścią,
        //wersja mało porządna
        var p = document.createElement('p');
        p.innerHTML = 'Treść akapitu.';
        document.body.appendChild(p);
    
        //tworzenie akapitu wraz z treścią,
        //wersja porządna
        var p = document.createElement('p');
        //tworzenie hiperłącza
        var a = document.createElement('a');
        //dodanie atrybutu
        a.setAttribute('href','http://wp.pl/');
        //tworzenie tekstu
        var aContent = document.createTextNode('Treść akapitu.');
        //wstawienie tekstu do hiperłącza
        a.appendChild(aContent);
        //wstawienie hiperłącza do akapitu
        p.appendChild(a);
        //wstawienie akapitu do body
        document.body.appendChild(p);
        //document.body.insertBefore(p, document.body.children[1]);
        //document.childNodes.unshift(p);
    
        //tabela
        var table = document.createElement('table');
    
        //wiersze
        var trNaglowek = document.createElement('tr');
        var trTresc = document.createElement('tr');
    
        //komórki
        var thNazwa = document.createElement('th');
        var thWartosc = document.createElement('th');
        var tdNazwa = document.createElement('td');
        var tdWartosc = document.createElement('td');
    
        //treść komórek
        thNazwa.innerHTML = 'Nazwa';
        thWartosc.innerHTML = 'Wartość';
        tdNazwa.innerHTML = 'Miejscowość';
        var input = document.createElement('input');
        input.setAttribute('value', 'Ustrzyki Górne');
        tdWartosc.appendChild(input);
    
        //wstawienie komórek do wierszy
        trNaglowek.appendChild(thNazwa);
        trNaglowek.appendChild(thWartosc);
        trTresc.appendChild(tdNazwa);
        trTresc.appendChild(tdWartosc);
    
        //wstawienie wierszy do tabeli
        table.appendChild(trNaglowek);
        table.appendChild(trTresc);
    
        //wstawienie tabeli do dokumentu
        document.body.appendChild(table);
        */
}
