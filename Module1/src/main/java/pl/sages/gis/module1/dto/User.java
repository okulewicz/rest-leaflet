//packaging is a way to organize source code and avoid names collision
//reverse domain naming is just a convention and the domains do not need to exist
package pl.sages.gis.module1.dto;

import com.sun.jdi.connect.Connector;

//class declaration
public class User {
    //fields hold a state of the object
    private String userName;
    private long id;

    //constructor - an operation to create an object of the class
    public User(long id, String userName) throws IllegalArgumentException {
        //verify if the users name follows certain rules
        if (userName.startsWith(" ")) {
            //if not - create an error
            throw new IllegalArgumentException("User names cannot start with empty characters");
        }
        this.userName = userName;
        this.id = id;
    }

    //a way to access userName
    public String getUserName() {
        return userName;
    }

    //a way to access id
    public long getId() {
        return id;
    }

    //a sample method
    public boolean hasAtLeast8Characters() {
        return userName.length() >= 8;
    }
}
