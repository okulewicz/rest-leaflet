package pl.sages.gis.module1;

import pl.sages.gis.module1.dto.User;

import java.util.ArrayList;
import java.util.List;

public class ConsoleApplication {
    public static void main(String[] args) {
        //a separate method for creating a collection
        List<User> users = createUsersList();
        //process the instances in a for-each loop
        for (User user: users) {
            checkUsernameLength(user);
        }
    }

    private static List<User> createUsersList() {
        List<User> users = new ArrayList<>();
        tryAddUserToList(users, 1L, "johnny123");
        tryAddUserToList(users, 2L, "ann123");
        tryAddUserToList(users, 3L, " ");
        return users;
    }

    private static void tryAddUserToList(List<User> users, long id, String name) {
        try {
            User user = new User(id,name);
            users.add(user);
            //process the possible error during execution
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static void checkUsernameLength(User user) {
        StringBuilder usernameLengthSB = new StringBuilder();
        usernameLengthSB.append("Username ");
        usernameLengthSB.append(user.getUserName());
        if (user.hasAtLeast8Characters()) {
            usernameLengthSB.append(" has at least 8 characters.");
        } else {
            usernameLengthSB.append(" does not have at least 8 characters.");
        }
        System.out.println(usernameLengthSB.toString());
    }
}
