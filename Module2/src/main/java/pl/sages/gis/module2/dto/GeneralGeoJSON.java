package pl.sages.gis.module2.dto;

import java.util.HashMap;
import java.util.Map;

public class GeneralGeoJSON {
    public String type = "Feature";
    public Map<String,Object> properties = new HashMap();
    public Map<String,Object> geometry;
}
