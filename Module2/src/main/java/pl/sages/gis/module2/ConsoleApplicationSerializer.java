package pl.sages.gis.module2;

import com.google.gson.Gson;
import pl.sages.gis.module2.dto.GeneralGeoJSON;
import pl.sages.gis.module2.dto.User;

public class ConsoleApplicationSerializer {
    public static void main(String[] args) {
        User user = new User(1L,"johnny123");
        System.out.println(user.serializeToJson());
        System.out.println(user.serializeToPrettyJson());

        GeneralGeoJSON gjson = new GeneralGeoJSON();
        gjson.properties.put("id", 2);
        gjson.properties.put("name","dom");
        Gson gson = new Gson();
        System.out.println(gson.toJson(gjson));

    }
}
