package pl.sages.gis.module2;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;
import pl.sages.gis.module2.dto.GeneralGeoJSON;

import static org.junit.Assert.*;

public class ConsoleApplicationSerializerTest {

    @Test
    public void testDeserializeGeoJSON() {
        String geoJSON = "{\"type\":\"Feature\",\"properties\":{\"name\":\"dom\",\"id\":2}}";
        Gson gson = new Gson();
        GeneralGeoJSON object = gson.fromJson(geoJSON, GeneralGeoJSON.class);
        Assert.assertEquals(2.0, object.properties.get("id"));
        Assert.assertEquals("dom", object.properties.get("name"));
        String realGeoJSON = "{\"type\": \"Feature\", \"geometry\": {\"type\":\"Point\",\"coordinates\":[25.246582,50.708634]}, \"properties\": {\"id\": 4, \"name\": \"fabryka\"}}";
        GeneralGeoJSON realObject = gson.fromJson(realGeoJSON, GeneralGeoJSON.class);
        Assert.assertEquals(4.0, realObject.properties.get("id"));
        Assert.assertEquals("fabryka", realObject.properties.get("name"));
        Assert.assertEquals("Point", realObject.geometry.get("type"));

    }
}