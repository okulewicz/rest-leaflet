package pl.sages.gis.module4.services;

import io.micronaut.context.annotation.Primary;
import pl.sages.gis.module4.dto.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Singleton;
import java.util.List;

@Primary
@Singleton
public class MockUserService implements UserService {
    private List<User> users;

    @Override
    public List<User> getUsers() {
        return users;
    }
    @Override
    public long addUser(String userName) {
        long newId = 0L;
        for (User user: users) {
            newId = Math.max(user.getId(),newId);
        }
        newId++;
        users.add(new User(newId, userName));
        return newId;
    }

    @PostConstruct
    public void initialize() {
        users = User.generateUsersList();
    }

    @PreDestroy
    public void cleanup() {
        //Do nothing for now
    }
}
