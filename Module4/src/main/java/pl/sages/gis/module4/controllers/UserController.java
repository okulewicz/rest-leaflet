package pl.sages.gis.module4.controllers;

import com.sun.jdi.connect.Connector;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import pl.sages.gis.module4.dto.User;
import pl.sages.gis.module4.services.UserService;

import java.beans.beancontext.BeanContext;
import java.util.List;

//this annotation defines the address where the controller will be made available
@Controller("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //this annotation marks the HTTP method mapped to the Java method
    @Get
    HttpResponse<List<User>> getUsers() {
        //we wrap up the users list within HTTP Response to get HTTP Status Code
        return HttpResponse.ok(userService.getUsers());
    }

    @Post
    HttpResponse<Long> postUser(@Body User user) {
        return HttpResponse.created(userService.addUser(user.getUserName()));
    }
}
