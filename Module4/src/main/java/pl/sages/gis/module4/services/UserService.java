package pl.sages.gis.module4.services;

import pl.sages.gis.module4.dto.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
    long addUser(String userName);
}
