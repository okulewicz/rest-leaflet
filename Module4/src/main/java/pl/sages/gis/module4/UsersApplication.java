package pl.sages.gis.module4;

import io.micronaut.runtime.Micronaut;

public class UsersApplication {

    public static void main(String[] args) {
        Micronaut.run(UsersApplication.class, args);
    }
}
