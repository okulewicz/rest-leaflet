//this sets initializeApplication an entry point for javascript application
document.addEventListener( "DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    logUsersList();
    presentUsersList();
    //attach possiblity of sending data to server
    document.getElementById('add-user').onclick = addUser;
}

function logUsersList() {
    fetch('users')
        //gets the JSON out of the response and prints it in the browsers console
        .then(response => response.json())
        .then(data => console.info(data));
}

function presentUsersList()  {
    //downloads the resource available under the address users
    fetch('users')
        //gets the JSON out of the response
        .then(response => response.json())
        //passes the JSON object to be processed
        .then(data => generateTable(data));
}

function generateTable(usersArray) {
    var placeholder = document.getElementById('users-placeholder');
    placeholder.innerHTML = '';
    //creates an HTML table, while knowing the structure of JSON data
    var tableUsers = document.createElement('table');
    //iterates over the array of users
    for (var i = 0; i < usersArray.length; ++i) {
        var trUser = generateRow(usersArray[i]);
        //adds row to the table
        tableUsers.appendChild(trUser);
        tableUsers.id = 'users-table';
    }
    //adds table to the HTML document
    placeholder.appendChild(tableUsers);
}

function generateRow(user) {
    //creates HTML elements
    var trUser = document.createElement('tr');
    var tdId = document.createElement('td');
    var tdName = document.createElement('td');
    //creates text elements
    var textId = document.createTextNode(user.id);
    var textName = document.createTextNode(user.userName);
    //constructs a single row
    tdId.appendChild(textId);
    tdName.appendChild(textName);
    trUser.appendChild(tdId);
    trUser.appendChild(tdName);
    return trUser;
}

function addUser() {
    //get username from input
    var username = document.getElementById('username').value;
    fetch('users',
    {
        method: 'POST',
        body: JSON.stringify({id: -1, userName: username}),
        headers: {
            'Content-Type': 'application/json'
          },
        //this will come in handy later
        credentials: 'include',
        mode: 'cors'
    })
    .then(function(response) {
        if (response.ok) {
        //reload users list
            presentUsersList();
        }
     });
}