var redIcon = new L.icon({
    iconUrl: 'styles/images/red.png',
    iconSize: [18, 30],
    iconAnchor: [9, 30],
    popupAnchor: [0, -30],
});

var orangeIcon = new L.icon({
    iconUrl: 'styles/images/orange.png',
    iconSize: [18, 29],
    iconAnchor: [9, 29],
    popupAnchor: [0, -29],
});

function propertiesTableGenerator(marker, feature) {
    return function() {
        var dataContainer = document.getElementById('data-container');
        dataContainer.innerHTML = '';
        var table = document.createElement('table');
        var tr = document.createElement('tr');
        var propertyTh = document.createElement('th');
        var propertyValueTh = document.createElement('th');
        var propertyName = document.createTextNode('Atrybut');
        var propertyValue = document.createTextNode('Wartość');
        propertyTh.appendChild(propertyName);
        propertyValueTh.appendChild(propertyValue);
        tr.appendChild(propertyTh);
        tr.appendChild(propertyValueTh);
        table.appendChild(tr);
        for (var property in feature.properties) {
            var tr = document.createElement('tr');
            var propertyTd = document.createElement('td');
            var propertyValueTd = document.createElement('td');
            var propertyValueInput = document.createElement('input');
            var propertyName = document.createTextNode(property);
            propertyTd.appendChild(propertyName);
            propertyValueInput.value = feature.properties[property];
            propertyValueInput.onchange = function (event) {
                console.log(event);
                feature.properties[property] = this.value;
                styleMarkerByFeature(marker, feature);
            }
            propertyValueTd.appendChild(propertyValueInput);
            tr.appendChild(propertyTd);
            tr.appendChild(propertyValueTd);
            table.appendChild(tr);
        }
        dataContainer.appendChild(table);
    };
}

function styleMarkerByFeature(marker, feature) {
    if (feature.properties.id == 1) {
        marker.setIcon(redIcon);
    } else {
        marker.setIcon(orangeIcon);
    }
}

function pointMarkerStyleById(feature, latlng) {
    var marker = L.marker(latlng);
    styleMarkerByFeature(marker, feature);
    marker.on('click', propertiesTableGenerator(marker, feature));
    return marker;
}

function displayPointId(feature, layer) {
    var popupContent = '<h1 class="leaflet-popup-content_header">I am a GeoJSON ' +
				feature.geometry.type + '</h1>';
	if (feature.properties && feature.properties.id) {
			popupContent += '<div class="leaflet-popup-content_body"><strong>Id:</strong> ' + feature.properties.id + '</div>';
		}
	layer.bindPopup(popupContent);
    }

//this sets initializeApplication an entry point for javascript application
document.addEventListener( "DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    loadMap();
}

function loadMap() {
    var mymap = L.map('map-container').setView([52.05, 20.00], 6);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    }).addTo(mymap);

    fetch('./points')
        .then(response => response.json())
        .then(data => addPointsToMap(data, mymap));
}

function addPointsToMap(data, mymap) {
    var pointsLayer = L.geoJSON(data, {
        pointToLayer: pointMarkerStyleById,
        onEachFeature: displayPointId
    });
    pointsLayer.addTo(mymap);
}

/*
function styleDefininition(feature) {
    switch (feature.properties.id) {
        case 1: return {color: "red"};
        case 2: return {color: "blue"};
    }
}
*/