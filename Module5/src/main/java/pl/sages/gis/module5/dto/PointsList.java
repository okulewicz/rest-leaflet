package pl.sages.gis.module5.dto;

import java.util.ArrayList;
import java.util.List;

public class PointsList {
    private final String type = "FeatureCollection";

    public String getType() {
        return type;
    }

    public List<Point> getFeatures() {
        return features;
    }

    private final List<Point> features = new ArrayList<>();

    public void addPoint(Point point) {
        features.add(point);
    }

}
