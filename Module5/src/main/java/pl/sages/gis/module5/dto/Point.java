package pl.sages.gis.module5.dto;

public class Point {
    public class Properties {
        private long id;

        public long getId() {
            return id;
        }
    }

    public class Geometry {
        private final String type = "Point";
        private double[] coordinates;

        public String getType() {
            return type;
        }

        public double[] getCoordinates() {
            return coordinates;
        }
    }

    private final String type = "Feature";
    private Properties properties;
    private Geometry geometry;

    public String getType() {
        return type;
    }

    public Properties getProperties() {
        return properties;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public Point(long id, double lat, double lng) {
        this.properties = new Properties();
        properties.id = id;
        this.geometry = new Geometry();
        this.geometry.coordinates = new double[2];
        this.geometry.coordinates[0] = lng;
        this.geometry.coordinates[1] = lat;
    }
}
