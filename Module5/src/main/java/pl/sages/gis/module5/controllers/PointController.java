package pl.sages.gis.module5.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import pl.sages.gis.module5.dto.Point;
import pl.sages.gis.module5.dto.PointsList;

@Controller("/points")
public class PointController {
    @Get
    public HttpResponse<PointsList> getAllPoints() {
        PointsList points = new PointsList();
        points.addPoint(new Point(1, 52,14));
        points.addPoint(new Point(2, 53,15));
        return HttpResponse.ok(points);
    }
}
