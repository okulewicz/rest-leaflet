package pl.sages.gis.module3;

import io.micronaut.runtime.Micronaut;

public class MicronautDemoApplication {

    public static void main(String[] args) {
        Micronaut.run(MicronautDemoApplication.class, args);
    }
}
