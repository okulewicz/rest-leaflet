//packaging is a way to organize source code and avoid names collision
//reverse domain naming is just a convention and the domains do not need to exist
package pl.sages.gis.module3.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

//class declaration
public class User {
    //fields hold a state of the object
    private String userName;
    private long id;

    //constructor - an operation to create an object of the class
    public User(long id, String userName) throws IllegalArgumentException {
        //verify if the users name follows certain rules
        if (userName.startsWith(" ")) {
            //if not - create an error
            throw new IllegalArgumentException("User names cannot start with empty characters");
        }
        this.userName = userName;
        this.id = id;
    }

    //a way to access userName
    public String getUserName() {
        return userName;
    }

    //a way to access id
    public long getId() {
        return id;
    }

    //a sample method
    public boolean hasAtLeast8Characters() {
        return userName.length() >= 8;
    }

    //serialize as condensed JSON
    public String serializeToJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    //serialize to extended print JSON
    public String serializeToPrettyJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public static List<User> generateUsersList() {
        List<User> users = new ArrayList<>();
        tryAddUserToList(users, 1L, "johnny123");
        tryAddUserToList(users, 2L, "ann123");
        return users;
    }

    private static void tryAddUserToList(List<User> users, long id, String name) {
        try {
            User user = new User(id,name);
            users.add(user);
            //process the possible error during execution
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
