package pl.sages.gis.module3.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import pl.sages.gis.module3.dto.User;

//this annotation defines the address where the controller will be made available
@Controller("/users")
public class UserController {
    //this annotation marks the HTTP method mapped to the Java method
    @Get()
    HttpResponse getUsers() {
        //we wrap up the users list within HTTP Response to get HTTP Status Code
        return HttpResponse.ok(User.generateUsersList());
    }
}
