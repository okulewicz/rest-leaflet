//this sets initializeApplication an entry point for javascript application
document.addEventListener( "DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    testConnection();
}

function testConnection() {
    fetch('test')
        .then(response => response.text())
        .then(function(text) {
            console.log(text);
            document.getElementById('data-container').innerHTML = text;
        });
}
