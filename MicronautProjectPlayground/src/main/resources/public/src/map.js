//this sets initializeApplication an entry point for javascript application
document.addEventListener( "DOMContentLoaded", initializeApplication, false);

function initializeApplication() {
    loadMap();
}

function loadMap() {
    var mymap = L.map('map-container', {editable: true}).setView([52.05, 20.00], 6);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>' +
        ' contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 19,
        id: 'osm.tiles'
    }).addTo(mymap);

    var polyline = L.polyline([[53.1, 19], [52, 20],[51, 20]]).addTo(mymap);
    polyline.enableEdit();
    mymap.on('editable:vertex:dragend', function (e) {
            e.layer.setStyle({color: 'DarkRed'});
            console.log(e.vertex.latlngs);
            var geoJSON = e.layer.toGeoJSON();
            geoJSON.properties.id = 1;
            geoJSON.properties.name = 'Ciekawa łamana';
            console.log(geoJSON);
            fetch('test',{
                body: JSON.stringify(geoJSON),
                method: 'POST',
                headers: {
                            'Content-Type': 'application/json'
                    }
                });
        });
    polyline.on('click', function (e) {
        console.log(e.target.toGeoJSON());
    });
}
