package pl.sages.gis.test;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/test")
public class TestController {
    @Get
    HttpResponse<String> test() {
        return HttpResponse.ok("REST API working");
    }
}
