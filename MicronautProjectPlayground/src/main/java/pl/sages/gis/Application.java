package pl.sages.gis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.micronaut.runtime.Micronaut;

import java.sql.*;

public class Application {
    static class DummyClass {
        int id;
        String value;
    }

    private static final String CONNECTION_STR = "jdbc:postgresql://localhost:5432/rest_leaflet_db";
    private static final String USERNAME = "rest_gis";
    private static final String PASSWORD = "rest-gis";
    public static final String SELECT_CONNECTION_SUCCESSFUL = "SELECT 'Connection successful'";

    public static void main(String[] args) {
        testConnection();
        testSerialization();
        Micronaut.run(Application.class, args);
    }

    private static void testConnection() {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STR, USERNAME, PASSWORD)) {
            // create a Statement
            try (Statement stmt = conn.createStatement()) {
                //execute query
                try (ResultSet rs = stmt.executeQuery(SELECT_CONNECTION_SUCCESSFUL)) {
                    //position result to first
                    rs.next();
                    System.out.println(rs.getString(1));
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    private static void testSerialization() {
        DummyClass dummyClass = new DummyClass();
        dummyClass.id = 1;
        dummyClass.value = "qwertyuiop";
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(dummyClass));
    }
}
