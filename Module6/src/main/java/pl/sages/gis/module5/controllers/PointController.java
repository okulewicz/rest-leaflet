package pl.sages.gis.module5.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import pl.sages.gis.module5.dto.Point;
import pl.sages.gis.module5.dto.PointsList;

import javax.validation.constraints.Null;

@Controller("/points")
public class PointController {
    @Get
    public HttpResponse<PointsList> getAllPoints() {
        PointsList points = new PointsList();
        points.addPoint(new Point(1, "fabryka", 52,14));
        points.addPoint(new Point(2, "dom", 53,15));
        return HttpResponse.ok(points);
    }

    @Put("/{id}")
    public HttpResponse<String> modifyPoint(@Body Point point, @QueryValue long id, @QueryValue String token ) {
        if ("0123456789".equals(token)) {
            StringBuilder pointStringBuilder = new StringBuilder();
            pointStringBuilder.append("Point received.\n");
            pointStringBuilder.append("id=");
            pointStringBuilder.append(point.properties.id);
            pointStringBuilder.append("\n");
            pointStringBuilder.append("name=");
            pointStringBuilder.append(point.properties.name);
            pointStringBuilder.append("\n");
            pointStringBuilder.append("lat=");
            pointStringBuilder.append(point.geometry.coordinates[1]);
            pointStringBuilder.append("\n");
            pointStringBuilder.append("lng=");
            pointStringBuilder.append(point.geometry.coordinates[0]);
            System.out.println(pointStringBuilder.toString());
            return HttpResponse.ok("Point received");
        } else {
            return HttpResponse.unauthorized();
        }
    }
}
